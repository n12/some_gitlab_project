import java.util.Random;

public class Main {

    public static void main(String[] args) {
        System.out.println(getEvenNumber1());
        System.out.println(getEvenNumber2());
    }

    public static int getEvenNumber1() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        return a * b;
    }

    public static int getEvenNumber2() {
        Random random = new Random();
        int a = random.nextInt(5000);
        return 2 * a;
    }
}
