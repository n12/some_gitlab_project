BODY="{
  \"title\": \"Fix the bug; commit: ${CI_COMMIT_SHA}\",
  \"labels\": \"bug\",
  \"assignee_ids\": [ ${GITLAB_USER_ID} ],
  \"description\": \"Branch: ${CI_COMMIT_REF_NAME}, commit, that arises the bug: ${CI_COMMIT_SHA}\"
}";

curl -X POST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/issues" \
  --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";
